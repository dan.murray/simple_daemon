#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

int main( int argc, char** argv ) {
	pid_t pid = fork();

	if ( pid < 0 ) {
		exit( EXIT_FAILURE );
	} else if ( pid > 0 ) {
		exit( EXIT_SUCCESS );
	}

	if ( setsid() < 0 ) {
		exit( EXIT_FAILURE );
	}

	signal( SIGCHLD, SIG_IGN );
	signal( SIGHUP, SIG_IGN );

	pid = fork();
	if ( pid > 0 ) {
		exit( EXIT_SUCCESS );
	}

	umask( 0 );
	chdir( "/" );

	for ( int i = sysconf( _SC_OPEN_MAX ); i >= 0; --i ) {
		close( i );
	}

	openlog( "daemon", LOG_PID, LOG_DAEMON );
	syslog( LOG_NOTICE, "daemon started." );

	sleep( 20 );

	syslog( LOG_NOTICE, "daemon terminated." );
	closelog();

	return EXIT_SUCCESS;
}

